// import { StatusBar } from "expo-status-bar";
import "react-native-gesture-handler";
import React from "react";

import User from "./Page/User";
import UserProvider from "./Provider/UserProvider";
import NavigationRoot from "./Routes/NavigationRoot";
import InputProvider from "./Provider/InputProvider";

export default function App() {
  return (
    <UserProvider>
      <InputProvider>
        <NavigationRoot />
      </InputProvider>
    </UserProvider>
  );
}
