import React, {createContext, memo, useReducer, useMemo} from 'react';

const globalContext = createContext({});

const GlobalProvider = memo(props => {
  const initialGlobalState = {
    globalValue: '',
    isValidGlobalValue: true,
  };

  const globalReducer = (prevState, action) => {
    switch (action.type) {
      case 'globalValueChange':
        if (action.text.length >= 4) {
          return {
            ...prevState,
            globalValue: action.text,
            isValidGlobalValue: true,
          };
        } else {
          return {
            ...prevState,
            globalValue: action.text,
          };
        }
      case 'globalValueValidation':
        if (action.text.length >= 4) {
          return {
            ...prevState,
            isValidGlobalValue: true,
          };
        } else {
          return {
            ...prevState,
            isValidGlobalValue: false,
          };
        }
      case 'clearGlobal':
        return {
          ...initialGlobalState,
        };

      default:
        throw new Error('Unexpected type of action');
    }
  };

  const [globalState, dispatch] = useReducer(globalReducer, initialGlobalState);

  const globalContextValue = useMemo(
    () => ({
      globalState: globalState,
      onglobalValueChange: text => {
        dispatch({
          type: 'globalValueChange',
          text: text.toString().toUpperCase(),
        });
      },
      onGlobalValueValidation: text => {
        dispatch({
          type: 'globalValueValidation',
          text,
        });
      },
      onClearGlobal: () => {
        dispatch({
          type: 'clearGlobal',
        });
      },
    }),
    [globalState, dispatch],
  );

  return (
    <globalContext.Provider value={globalContextValue}>
      {props.children}
    </globalContext.Provider>
  );
});

export default GlobalProvider;
export {globalContext};
