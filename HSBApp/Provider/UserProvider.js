import React, { createContext, memo, useReducer, useMemo } from "react";

const userContext = createContext({});

const UserProvider = memo((props) => {
  const initialUserState = {
    userToken: null,
  };

  const userReducer = (prevState, action) => {
    switch (action.type) {
      case "setToken":
        return {
          ...prevState,
          userToken: action.text,
        };
      case "clearUser":
        return {
          ...initialUserState,
        };

      default:
        throw new Error("Unexpected type of action");
    }
  };

  const [userState, dispatch] = useReducer(userReducer, initialUserState);

  const userContextValue = useMemo(
    () => ({
      userState: userState,
      onSetToken: (text) => {
        dispatch({
          type: "setToken",
          text: text,
        });
      },
      onClearUser: () => {
        dispatch({
          type: "clearUser",
        });
      },
    }),
    [userState, dispatch]
  );

  return (
    <userContext.Provider value={userContextValue}>
      {props.children}
    </userContext.Provider>
  );
});

export default UserProvider;
export { userContext };
