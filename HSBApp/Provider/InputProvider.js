import React, { createContext, memo, useReducer, useMemo } from "react";

const inputContext = createContext({});

const InputProvider = memo((props) => {
  const initialInputState = {
    name: "",
    isValidName: true,
    password: "",
    isValidPassword: true,
    passwordConf: "",
    isValidPasswordConf: true,
  };

  const inputReducer = (prevState, action) => {
    switch (action.type) {
      case "nameChange":
        if (action.text.length >= 4) {
          return {
            ...prevState,
            name: action.text,
            isValidName: true,
          };
        } else {
          return {
            ...prevState,
            name: action.text,
          };
        }
      case "nameValidation":
        if (action.text.length >= 4) {
          return {
            ...prevState,
            isValidName: true,
          };
        } else {
          return {
            ...prevState,
            isValidName: false,
          };
        }
      case "passwordChange":
        if (action.text.length >= 4) {
          return {
            ...prevState,
            password: action.text,
            isValidPassword: true,
          };
        } else {
          return {
            ...prevState,
            password: action.text,
          };
        }
      case "passwordValidation":
        if (action.text.length >= 4) {
          return {
            ...prevState,
            isValidPassword: true,
          };
        } else {
          return {
            ...prevState,
            isValidPassword: false,
          };
        }
      case "passwordConfChange":
        if (action.text.length >= 4) {
          return {
            ...prevState,
            passwordConf: action.text,
            isValidPasswordConf: true,
          };
        } else {
          return {
            ...prevState,
            passwordConf: action.text,
          };
        }
      case "passwordConfValidation":
        if (action.text.length >= 4) {
          return {
            ...prevState,
            isValidPasswordConf: true,
          };
        } else {
          return {
            ...prevState,
            isValidPasswordConf: false,
          };
        }

      case "clearInput":
        return {
          ...initialInputState,
        };

      default:
        throw new Error("Unexpected type of action");
    }
  };

  const [inputState, dispatch] = useReducer(inputReducer, initialInputState);

  const inputContextValue = useMemo(
    () => ({
      inputState: inputState,
      onNameChange: (text) => {
        dispatch({
          type: "nameChange",
          text: text,
        });
      },
      onNameValidation: (text) => {
        dispatch({
          type: "nameValidation",
          text,
        });
      },
      onPasswordChange: (text) => {
        dispatch({
          type: "passwordChange",
          text: text,
        });
      },
      onPasswordValidation: (text) => {
        dispatch({
          type: "passwordValidation",
          text,
        });
      },
      onPasswordConfChange: (text) => {
        dispatch({
          type: "passwordConfChange",
          text: text,
        });
      },
      onPasswordConfValidation: (text) => {
        dispatch({
          type: "passwordConfValidation",
          text,
        });
      },
      onClearInput: () => {
        dispatch({
          type: "clearInput",
        });
      },
    }),
    [inputState, dispatch]
  );

  return (
    <inputContext.Provider value={inputContextValue}>
      {props.children}
    </inputContext.Provider>
  );
});

export default InputProvider;
export { inputContext };
