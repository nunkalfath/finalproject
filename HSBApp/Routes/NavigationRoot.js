//Dependencies Modules
import "react-native-gesture-handler";
import React, { memo, useContext, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

//Component Modules
// import { userContext } from "../Provider/UserProvider";
import Register from "../Page/Register/Register";
import Login from "../Page/Login";
import { userContext } from "../Provider/UserProvider";

const AuthStack = createStackNavigator();
// const MainStack = createStackNavigator();

const NavigationRoot = memo((props) => {
  const {
    userState: { userToken },
  } = useContext(userContext);

  {
    if (userToken) {
      return (
        <NavigationContainer>
          <MainStack.Navigator headerMode="none">
            <MainStack.Screen name="Home" component={MainTabScreen} />
          </MainStack.Navigator>
        </NavigationContainer>
      );
    }
    {
      return (
        <NavigationContainer>
          <AuthStack.Navigator headerMode="none" initialRouteName="Greeting">
            <AuthStack.Screen name="Register" component={Register} />
            <AuthStack.Screen name="Login" component={Login} />
          </AuthStack.Navigator>
        </NavigationContainer>
      );
    }
  }
});

export default NavigationRoot;
