import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 150,
    backgroundColor: "white",
    paddingTop: 10,
    alignItems: "center",
    // elevation: 10,
  },
  helloReg: {
    // paddingBottom:0,
    justifyContent: "center",
  },
  welCome: {
    paddingBottom: 10,
    fontSize: 36,
    fontWeight: "bold",
    fontFamily: "normal",
    alignSelf: "center",
  },
  isiPlease: {
    paddingBottom: 20,
    fontSize: 12,
    fontFamily: "normal",
    alignSelf: "center",
  },
  regForm: {
    marginTop: 10,
    alignSelf: "center",
    width: "85%",
  },
  inputView: {
    height: 45,
    backgroundColor: "white",
    borderColor: "black",
    borderWidth: 1,
    borderRadius: 7,
    marginTop: 20,
    justifyContent: "center",
    padding: 20,
  },
  inputText: {
    height: 50,
    color: "black",
  },
  buttonReg: {
    flex: 1,
    flexDirection: "row",
    marginTop: 40,
  },
  firstButton: {
    width: "40%",
    height: 45,
    marginHorizontal: 15,
    backgroundColor: "#DADADA",
    borderColor: "#DADADA",
    borderWidth: 1,
    borderRadius: 7,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  secondButton: {
    width: "40%",
    height: 45,
    marginHorizontal: 15,
    backgroundColor: "#DE40B2",
    borderColor: "#DE40B2",
    borderWidth: 1,
    borderRadius: 7,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  textStyle: {
    color: "white",
  },
});

export default styles;
