import React, { useContext } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import styles from "../styles";
import { inputContext } from "../../Provider/InputProvider";

const Register = (props) => {
  const {
    inputState,
    onNameChange,
    onNameValidation,
    onPasswordChange,
    onPasswordValidation,
    onPasswordConfChange,
    onPasswordConfValidation,
  } = useContext(inputContext);
  console.log({ inputState: inputState });
  const {
    name,
    isValidName,
    password,
    isValidPassword,
    passwordConf,
  } = inputState;
  const {
    navigation: { navigate },
  } = props;

  // const value = [{ name }, { password }];
  const storeLoginData = async () => {
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem("reg_key", jsonValue);
    } catch (e) {
      console.log(e);
    }
  };
  const keLogin = () => {
    navigate("Login");
  };

  const onRegister = () => {
    storeLoginData();
    keLogin();
  };
  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image
            source={require("../../assets/image/logo.png")}
            style={{
              width: 100,
              height: 100,
              borderRadius: 0,
            }}
          />
        </View>
        <View style={styles.helloReg}>
          <Text style={styles.welCome}>Welcome!</Text>

          <Text style={styles.isiPlease}>
            Please register or login if you have an account before.
          </Text>
          <View style={styles.regForm}>
            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholder="Email Address"
                value={name}
                placeholderTextColor="#7D7777"
                onChangeText={(text) => {
                  onNameChange(text);
                }}
                keyboardType="default"
                onEndEditing={(e) => onNameValidation(e.nativeEvent.text)}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholder="Create Password"
                placeholderTextColor="#7D7777"
                value={password}
                onChangeText={(text) => {
                  onPasswordChange(text);
                }}
                secureTextEntry={true}
                onEndEditing={(e) => onPasswordValidation(e.nativeEvent.text)}
              />
            </View>

            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholder="Password Confirmation"
                placeholderTextColor="#7D7777"
                value={passwordConf}
                onChangeText={(text) => {
                  onPasswordConfChange(text);
                }}
                secureTextEntry={true}
                onEndEditing={(e) =>
                  onPasswordConfValidation(e.nativeEvent.text)
                }
              />
            </View>
            <View style={styles.buttonReg}>
              <TouchableOpacity
                onPress={() => {
                  onRegister();
                }}
                style={styles.firstButton}
              >
                <Text>LOGIN</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.secondButton}>
                <Text style={styles.textStyle}>REGISTER</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Register;
