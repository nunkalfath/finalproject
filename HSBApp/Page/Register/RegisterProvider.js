import React, {
  useContext,
  createContext,
  memo,
  useReducer,
  useMemo,
} from "react";
import { userContext } from "../../Provider/UserProvider";
import { inputContext } from "../../Provider/InputProvider";

const registerContext = createContext({});

const RegisterProvider = memo((props) => {
  const {
    navigation: { navigate },
  } = props;
  // note: globalContext Data
  const userValue = useContext(userContext);
  const inputValue = useContext(inputContext);
  const {
    userState: { userToken },
  } = userValue;
  const { inputState, onClearInput } = inputValue;

  const initialRegisterData = {};

  //action type menggunakan standard camelCase dalam bentuk string
  const registerReducer = (prevData, action) => {
    switch (action.type) {
      case "setProductID":
        return {
          ...prevData,
          product_id: action.product_id,
        };
      case "clearRegister":
        return {
          ...initialRegisterData,
        };

      default:
        throw new Error("Unexpected type of action");
    }
  };

  const [registerData, dispatch] = useReducer(
    registerReducer,
    initialRegisterData
  );

  const registerContextValue = useMemo(
    () => ({
      registerData: registerData,
      onSetProduct: (name, code) => {
        dispatch({ type: "setProduct", name, code });
      },
      onClearListrik: async () => {
        await onClearInput();
        dispatch({
          type: "clearRegister",
        });
      },
    }),
    [userValue, inputValue, registerData, dispatch]
  );

  return (
    <registerContext.Provider value={registerContextValue}>
      {props.children}
    </registerContext.Provider>
  );
});

export default RegisterProvider;
export { registerContext };
