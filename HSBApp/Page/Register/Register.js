import React, { memo } from "react";

//Component Modules
import RegisterPage from "./RegisterPage";
import RegisterProvider from "./RegisterProvider";

const Register = memo(({ navigation }) => {
  return (
    <RegisterProvider navigation={navigation}>
      <RegisterPage navigation={navigation} />
    </RegisterProvider>
  );
});

export default Register;
