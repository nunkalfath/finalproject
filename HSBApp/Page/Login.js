import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import styles from "./styles";
import { inputContext } from "../Provider/InputProvider";

const Login = (props) => {
  const {
    navigation: { navigate },
  } = props;
  const {
    inputState: { name, isValidName, password, isValidPassword },
    onNameChange,
    onNameValidation,
    onPasswordChange,
    onPasswordValidation,
    onPasswordConfChange,
    onPasswordConfValidation,
  } = useContext(inputContext);

  const keRegister = () => {
    navigate("Register");
  };

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image
            source={require("../assets/image/logo.png")}
            style={{
              width: 100,
              height: 100,
              borderRadius: 0,
            }}
          />
        </View>
        <View style={styles.helloReg}>
          <Text style={styles.welCome}>Login</Text>

          <View style={styles.regForm}>
            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholder="Email Address"
                value={name}
                placeholderTextColor="#7D7777"
                onChangeText={(text) => {
                  onNameChange(text);
                }}
                keyboardType="default"
                onEndEditing={(e) => onNameValidation(e.nativeEvent.text)}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholder="Create Password"
                placeholderTextColor="#7D7777"
                value={password}
                onChangeText={(text) => {
                  onPasswordChange(text);
                }}
                secureTextEntry={true}
                onEndEditing={(e) => onPasswordValidation(e.nativeEvent.text)}
              />
            </View>

            <View style={styles.buttonReg}>
              <TouchableOpacity
                onPress={() => {
                  keRegister();
                }}
                style={styles.firstButton}
              >
                <Text>REGISTER</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.secondButton}>
                <Text style={styles.textStyle}>LOGIN</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;
