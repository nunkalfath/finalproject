import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { AntDesign, Ionicons, Entypo, MaterialIcons } from "@expo/vector-icons";
// import { Ionicons } from "@expo/vector-icons";
// import { Entypo } from "@expo/vector-icons";
// import { MaterialIcons } from "@expo/vector-icons";

export default class User extends React.Component {
  render() {
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={styles.container}>
          <View style={styles.navBar}>
            <MaterialIcons name="account-circle" size={75} color="#DE40B2" />
            <View style={styles.textNavbar}>
              <Text style={styles.helloText}>Siti Nur Alfath</Text>
              <Text style={styles.findText}>Bergabung sejak 2015</Text>
            </View>
          </View>

          <Text style={styles.skincareText}>Contact</Text>
          <View style={styles.contactForm}>
            <View style={styles.iconText}>
              <AntDesign name="instagram" size={24} color="#DE40B2" />
              <Text style={styles.instaText}>
                Halwey Stokis Bandung (nunkalfathshop_halwey)
              </Text>
            </View>
            <View style={styles.iconText}>
              <Ionicons name="logo-whatsapp" size={24} color="#DE40B2" />
              <Text style={styles.instaText}>08880-978-2362</Text>
            </View>
            <View style={styles.iconText}>
              <Entypo name="shop" size={24} color="#DE40B2" />
              <Text style={styles.instaText}>Antapani, Kota Bandung</Text>
            </View>
          </View>
          <View style={styles.footForm}>
            <TouchableOpacity>
              <MaterialIcons name="account-circle" size={24} color="#DE40B2" />
            </TouchableOpacity>
            <TouchableOpacity>
              <MaterialIcons name="home" size={24} color="#DE40B2" />
            </TouchableOpacity>
            <TouchableOpacity>
              <MaterialIcons name="exit-to-app" size={24} color="#DE40B2" />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 150,
    backgroundColor: "white",
    paddingTop: 10,
    alignItems: "center",
    flexDirection: "row",
    // justifyContent: "space-around",
  },
  textNavbar: {},
  helloText: {
    fontSize: 24,
  },
  findText: {
    fontSize: 14,
  },
  skincareForm: {
    marginTop: 10,
    backgroundColor: "white",
    borderColor: "#DADADA",
    borderWidth: 1,
    height: 300,
    width: "95%",
    alignSelf: "center",
    marginBottom: 50,
  },
  skincareText: {
    marginTop: 20,
    fontSize: 24,
    fontFamily: "serif",
  },
  circleButton: {
    marginTop: 40,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },

  buttonForm: {
    position: "absolute",
    width: 50,
    height: 50,
    borderRadius: 30,
    backgroundColor: "#FAD4EF",
  },
  textCTMP: {
    color: "black",
    fontSize: 18,
    alignItems: "center",
  },
  contactForm: {
    marginTop: 10,
    backgroundColor: "white",
    borderColor: "#DADADA",
    borderWidth: 1,
    height: 150,
    width: "95%",
    alignSelf: "center",
    marginBottom: 50,
  },
  iconText: {
    marginTop: 10,
    marginLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  instaText: {
    marginLeft: 10,
    fontSize: 12,
    color: "#DE40B2",
  },

  footForm: {
    marginTop: 10,
    backgroundColor: "#FAD4EF",
    height: 50,
    width: "95%",
    alignSelf: "center",
    alignItems: "center",
    marginBottom: 50,
    flexDirection: "row",
    justifyContent: "space-around",
  },
});
